COMPILE = gcc
EXE = test
SOURCE = test.c
PTH = pthread

compile:
	$(COMPILE) -o $(EXE) $(SOURCE) -lm -lstdc++ -pthread
	
exe:
	./$(EXE)
