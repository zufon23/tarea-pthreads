# Tarea Pthreads
Darieth Fonseca Zuñiga, B62738
Jonathan Ramírez Hernández, B55730.

Síntesis.
Este proyecto sirve para determinar el producto matricial de una matriz de tamaño nxn donde n es un número aleatorio
entre 3 y 8. Esto se hace con una implementación en serie que puede ser entendida como una programación "estándar".
También, se utiliza una programación en paralelo en la que se utilizan threads.
En general, la programación con threads se encarga de asignarle una porción del trabajo total a cada thread, luego los threads se unen para obtener el resultado final del programa.
Este proyecto también sirve para determibnar una aprximación del valor de pi usando la serie de Gregory-Leibniz, cuya información puede ser consultada en el link adjunto. Esto, de igual manera, se realiza usando una implementación en serie y en paralelo. 
Para ambos ejercicios, se comparan los resultados de ambas implementaciones y se determina si resultan ser iguales o distintos.


Resultados del pipeline:
El programa test.c escribe dos archivos que se escriben con un valor de 0 si se ejecutaron ambas operaciones de manera correcta, si esto no ocurre, se escribe un 1. Sabiendo esto, se revisan si dichos resultados generan un valor de 00, lo cual significa que ambos archivos tienen un 0, por lo que se garantiza la correcta ejecución del programa.

Referencias.

Serie de Gregory-Leibniz.

https://mathworld.wolfram.com/LeibnizSeries.html